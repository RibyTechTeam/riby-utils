import BaseError from '../errors/base-error';
import ServerError from '../errors/server-error';
import { TErrorHandler } from '../types/error-handler';

const handleAppExceptions: TErrorHandler = (error, next) => {
    if (error instanceof BaseError) return next();

    return new ServerError(null, null, error);
};

export default handleAppExceptions;
