import BaseError from '../errors/base-error';
import { TErrorHandler } from '../types/error-handler';

const errorHandler = (...handlers: TErrorHandler[]) => {
    return (error: Error) => {
        const nextFn = (count: number): BaseError | null => {
            let calledNext = false;

            if (count === handlers.length) return null;

            const err = handlers[count](error, () => {
                calledNext = true;
            });

            if (calledNext) {
                return nextFn(count + 1);
            } else {
                if (!(err instanceof BaseError)) {
                    throw new Error('Error handler must return a base error');
                }

                return err;
            }
        };

        const derivedError = nextFn(0);

        if (derivedError) return derivedError;

        throw new Error('Handler not implemented');
    };
};

export default errorHandler;
