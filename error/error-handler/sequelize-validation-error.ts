import Sequelize from 'sequelize';
import ValidationError from '../errors/validation-error';
import { TErrorHandler } from '../types/error-handler';

const handleSequelizeValidationError: TErrorHandler = (error, next) => {
    if (!(error instanceof Sequelize.ValidationError)) return next();

    let message = error.errors.map((err) => err.message).join('|');

    return new ValidationError(message, null, error);
};

export default handleSequelizeValidationError;
