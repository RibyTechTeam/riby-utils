import BaseError from '../errors/base-error';
import { TErrorHandler } from '../types/error-handler';

const handleBaseError: TErrorHandler = (error, next) => {
    if (!(error instanceof BaseError)) return next();

    return error;
};

export default handleBaseError;
