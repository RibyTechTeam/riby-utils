import BaseError from '../errors/base-error';

export interface TErrorHandler {
    (error: Error, next: () => void): BaseError | void;
}
