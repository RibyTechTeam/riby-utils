import { TServiceExceptionCodes, EErrorCodes, EStatusCodes, EExceptionNames } from './types';

const defaultExceptions: TServiceExceptionCodes = {
    codes: {
        [EExceptionNames.BAD_REQUEST]: {
            code: EErrorCodes.BAD_REQUEST,
            statusCode: EStatusCodes.BAD_REQUEST,
        },
        [EExceptionNames.UNAUTHORIZED]: {
            code: EErrorCodes.UNAUTHORIZED,
            statusCode: EStatusCodes.UNAUTHORIZED,
        },
        [EExceptionNames.FORBIDDEN]: {
            code: EErrorCodes.FORBIDDEN,
            statusCode: EStatusCodes.FORBIDDEN,
        },
        [EExceptionNames.NOT_FOUND]: {
            code: EErrorCodes.NOT_FOUND,
            statusCode: EStatusCodes.NOT_FOUND,
        },
        [EExceptionNames.CONFLICT]: {
            code: EErrorCodes.CONFLICT,
            statusCode: EStatusCodes.CONFLICT,
        },
        [EExceptionNames.GONE]: {
            code: EErrorCodes.GONE,
            statusCode: EStatusCodes.GONE,
        },
        [EExceptionNames.SERVER_ERROR]: {
            code: EErrorCodes.SERVER_ERROR,
            statusCode: EStatusCodes.SERVER_ERROR,
        },
        [EExceptionNames.SERVICE_UNAVAILABLE]: {
            code: EErrorCodes.SERVICE_UNAVAILABLE,
            statusCode: EStatusCodes.SERVICE_UNAVAILABLE,
        },
    },
};

export default defaultExceptions;
