import { EErrorCodes, EExceptionNames, EStatusCodes, TServiceExceptionCodes } from './types';

const walletManagerExceptions: TServiceExceptionCodes = {
    codes: {
        [EExceptionNames.DEBIT_ALREADY_PROCESSED]: {
            code: EErrorCodes.DEBIT_ALREADY_PROCESSED,
            statusCode: EStatusCodes.CONFLICT,
        },
        [EExceptionNames.CREDIT_ALREADY_PROCESSED]: {
            code: EErrorCodes.CREDIT_ALREADY_PROCESSED,
            statusCode: EStatusCodes.CONFLICT,
        },
        [EExceptionNames.DEBIT_REFERENCE_NOT_FOUND]: {
            code: EErrorCodes.DEBIT_REFERENCE_NOT_FOUND,
            statusCode: EStatusCodes.NOT_FOUND,
        },
        [EExceptionNames.CREDIT_REFERENCE_NOT_FOUND]: {
            code: EErrorCodes.CREDIT_REFERENCE_NOT_FOUND,
            statusCode: EStatusCodes.NOT_FOUND,
        },
        [EExceptionNames.WALLET_LOCKED]: {
            code: EErrorCodes.WALLET_LOCKED,
            statusCode: EStatusCodes.FORBIDDEN,
        },
    },
};

export default walletManagerExceptions;
