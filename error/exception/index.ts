import defaultExceptions from './defaults';
import { TExceptionCodes } from './types';
import walletManagerExceptions from './wallet-manager';

const exceptions = {
    ...defaultExceptions.codes,
    ...walletManagerExceptions.codes,
} as TExceptionCodes['codes'];

export default exceptions;
export * from './types';
