import {
    errorHandler,
    handleSequelizeValidationError,
    handleBaseError,
    handleAppExceptions,
} from './error-handler';
import { TErrorHandler } from './types/error-handler';

const errorMiddleware = (...handlers: TErrorHandler[]) => {
    const handler = errorHandler(
        ...handlers,
        handleSequelizeValidationError,
        handleBaseError,
        handleAppExceptions,
    );

    return (err: Error, req: any, res: any, next: VoidFunction) => {
        const error = handler(err);

        const response = {
            statusCode: error.statusCode,
            responseCode: 0,
            responseText: error.message,
            error: {
                code: error.code,
            },
        };

        res.status(error.statusCode).json(response);
    };
};

export default errorMiddleware;
