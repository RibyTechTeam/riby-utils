import exceptions from '../exception';
import { EErrorCodes } from '../exception/types';
import BaseError from './base-error';

class ValidationError extends BaseError {
    constructor(message: string | null = null, code: EErrorCodes | null = null, origin: Error | null = null) {
        const exception = exceptions.BAD_REQUEST;

        super(origin, {
            ...exception,
            code: code ?? exception.code,
        });

        this.name = 'ValidationError';
        this.message = message ?? 'Validation error';

        Error.captureStackTrace(this, this.constructor);
    }
}

export default ValidationError;
