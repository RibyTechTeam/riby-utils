import exceptions from '../exception';
import ExceptionCodes from '../exception';
import { EErrorCodes } from '../exception/types';
import BaseError from './base-error';

class AuthorizationError extends BaseError {
    constructor(message: string | null = null, code: EErrorCodes | null = null, origin: Error | null = null) {
        const exception = exceptions.UNAUTHORIZED;

        super(origin, {
            ...exception,
            code: code ?? exception.code,
        });

        this.name = 'AuthorizationError';
        this.message = message ?? 'You are not authorized to perform this action';

        Error.captureStackTrace(this, this.constructor);
    }
}

export default AuthorizationError;
