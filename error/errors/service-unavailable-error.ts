import exceptions from '../exception';
import { EErrorCodes } from '../exception/types';
import BaseError from './base-error';

class ServiceUnavailableError extends BaseError {
    constructor(message: string | null = null, code: EErrorCodes | null = null, origin: Error | null = null) {
        const exception = exceptions.SERVICE_UNAVAILABLE;

        super(origin, {
            ...exception,
            code: code ?? exception.code,
        });

        this.name = 'ServiceUnavailableError';
        this.message = message ?? 'Service unavailable at the moment';

        Error.captureStackTrace(this, this.constructor);
    }
}

export default ServiceUnavailableError;
