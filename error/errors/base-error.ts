import exceptions from '../exception';
import { EErrorCodes, EStatusCodes } from '../exception/types';

class BaseError extends Error {
    statusCode: EStatusCodes;
    origin: Error | null;
    code: EErrorCodes;

    constructor(origin: Error | null = null, exception = exceptions.SERVER_ERROR) {
        super('Error');

        this.name = 'BaseError';
        this.statusCode = exception.statusCode;
        this.code = exception.code;
        this.origin = origin;

        Error.captureStackTrace(this, this.constructor);
    }
}

export default BaseError;
