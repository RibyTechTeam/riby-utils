import exceptions from '../exception';
import { EErrorCodes } from '../exception/types';
import BaseError from './base-error';

class NotImplementedError extends BaseError {
    constructor(message: string | null = null, code: EErrorCodes | null = null, origin: Error | null = null) {
        const exception = exceptions.SERVICE_UNAVAILABLE;

        super(origin, {
            ...exception,
            code: code ?? exception.code,
        });

        this.name = 'NotImplementedFailedError';
        this.message = message ?? 'Feature not implemented';

        Error.captureStackTrace(this, this.constructor);
    }
}

export default NotImplementedError;
