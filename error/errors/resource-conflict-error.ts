import exceptions from '../exception';
import { EErrorCodes } from '../exception/types';
import BaseError from './base-error';

class ResourceConflictError extends BaseError {
    constructor(message: string | null = null, code: EErrorCodes | null = null, origin: Error | null = null) {
        const exception = exceptions.CONFLICT;

        super(origin, {
            ...exception,
            code: code ?? exception.code,
        });

        this.name = 'ResourceConflictError';
        this.message = message ?? 'Resource already exists';

        Error.captureStackTrace(this, this.constructor);
    }
}

export default ResourceConflictError;
