import exceptions from '../exception';
import { EErrorCodes } from '../exception/types';
import BaseError from './base-error';

class ResourceNotFoundError extends BaseError {
    constructor(message: string | null = null, code: EErrorCodes | null = null, origin: Error | null = null) {
        const exception = exceptions.NOT_FOUND;

        super(origin, {
            ...exception,
            code: code ?? exception.code,
        });

        this.name = 'ResourceNotFoundError';
        this.message = message ?? 'Resource not found';

        Error.captureStackTrace(this, this.constructor);
    }
}

export default ResourceNotFoundError;
