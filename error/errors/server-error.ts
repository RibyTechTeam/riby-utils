import exceptions from '../exception';
import { EErrorCodes } from '../exception/types';
import BaseError from './base-error';

class ServerError extends BaseError {
    constructor(message: string | null = null, code: EErrorCodes | null = null, origin: Error | null = null) {
        const exception = exceptions.SERVER_ERROR;

        super(origin, {
            ...exception,
            code: code ?? exception.code,
        });

        this.name = 'ServerError';
        this.message = message ?? 'Server error';

        Error.captureStackTrace(this, this.constructor);
    }
}

export default ServerError;
