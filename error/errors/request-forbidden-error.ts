import exceptions from '../exception';
import { EErrorCodes } from '../exception/types';
import BaseError from './base-error';

class RequestForbiddenError extends BaseError {
    constructor(message: string | null = null, code: EErrorCodes | null = null, origin: Error | null = null) {
        const exception = exceptions.FORBIDDEN;

        super(origin, {
            ...exception,
            code: code ?? exception.code,
        });

        this.name = 'RequestForbiddenError';
        this.message = message ?? 'Request forbidden';

        Error.captureStackTrace(this, this.constructor);
    }
}

export default RequestForbiddenError;
