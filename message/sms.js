const val = require('validator');

/**
 * @desc sends an sms to the queue to be delivered to the recipients
 * @param {string | undefined} sender
 * @param {string} message
 * @param {string[]} recipients
 * @param {object} sqs
 * @param {string} queue
 * throws Error
 * @return {Promise}
 */

const send = async ({ senderName, sender, message, recipients }, {sqs, queue}) => {
    if (sender && !val.isLength(sender, {min: 1, max: 11}) ) {
        throw new Error('sender must be a string of not more than 11 characters');
    }

    if (senderName && !val.isLength(senderName, { min: 1, max: 11 })) {
        throw new Error('senderName must be a string of not more than 11 characters');
    }

    if ( !val.isLength(message, {min: 1, max: 1000}) ){
        throw new Error('message must be a string of not more than 200 characters')
    }

    if (!Array.isArray(recipients) || recipients.length < 1) {
        throw new Error('recipients must be an array of mobile numbers')
    }

    if (!recipients.every(i => /^0([0-9]{10})/.test(i))) {
        throw new Error('recipients contains invalid phone numbers');
    }

    const recipientsStr = recipients.join(',');

    const params = {
        MessageBody: JSON.stringify({ senderName, sender, message, recipients: recipientsStr }),
        QueueUrl: queue,
        MessageAttributes: {
            messageType: {
                DataType: 'String',
                StringValue: 'sms'
            }
        }
    };

    let result;

    try {
        result = await sendToQueue(params, sqs);
    } catch (err) {
        throw err;
    }

    return result;
};

const sendToQueue = (params, sqs) => {
    return new Promise((resolve, reject) => {
        sqs.sendMessage(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

module.exports = send;
