'use strict';

const admin = require('firebase-admin');
const Request = require('request-promise');
const getAuth = require('../utils/basic-auth-helper');

const validateCredentials = (credentials) => {
    if (!credentials) return false;

    const cred = JSON.parse(credentials);
    const keys = ['type', 'project_id', 'private_key_id', 'private_key', 'client_email', 'client_id', 'auth_uri', 'token_uri', 'auth_provider_x509_cert_url', 'client_x509_cert_url'];
    const hasNullKey = keys.find(key => !cred[key]);

    if (hasNullKey) return false;
    return cred;
}

const sendNotification = (app, eventType, message, tokens, meta) => {
    return app.messaging().sendMulticast({
        tokens: tokens.map(token => token.token),
        notification: {
            title: message.title,
            body: message.body,
        },
        data: {
            title: message.title,
            body: message.body,
            type: eventType,
            created: new Date().toISOString(),
            ...meta,
        }
    });
};

const sendMessage = async ({ eventType, message, recipients = [], deviceTokenUrl, basicAuth, app, broadcast = false, meta }) => {
    const Authorization = getAuth(basicAuth);
    let shouldFetch = true;
    let page = 1;
    let total = null;

    while (shouldFetch) {
        if (!broadcast && !recipients.length) return true;

        const payload = broadcast ? {} : {
            body: {
                identifier_type: 'phone_number',
                identifiers: recipients,
                limit: 100,
                page,
            }
        };

        let tokens = await Request(`${deviceTokenUrl}/device/get-device-tokens?page=${page}&limit=100`, {
            method: broadcast ? 'GET' : 'POST',
            headers: { Authorization },
            ...payload,
            json: true,
        }).then(res => res.payload)

        tokens.tokens.length && sendNotification(app, eventType, message, tokens.tokens, meta);

        if (total === null) total = tokens.total;

        shouldFetch = tokens.pages > page;

        page++;
    }

    return true;
}

const initialize = (googleAppCredentials) => {
    const credentials = validateCredentials(googleAppCredentials);

    if (!credentials) return null;

    const { ...appCredentials } = credentials;
    const app = admin.initializeApp({
        credential: admin.credential.cert({ ...appCredentials }),
    });

    const sendPushNotifications = ({ eventType, message, deviceTokenUrl, basicAuth, meta = {} }) => {
        return {
            send(recipients) {
                return sendMessage({ eventType, message, recipients, deviceTokenUrl, basicAuth, app, meta });
            },

            broadcast() {
                return sendMessage({ eventType, message, deviceTokenUrl, basicAuth, app, broadcast: true, meta });
            },
        };
    };

    return sendPushNotifications;
};

module.exports = initialize;
