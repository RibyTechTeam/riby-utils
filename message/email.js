const val = require('validator');

/**
 * @desc sends an sms to the queue to be delivered to the recipients
 * @param {string} subject the subject of the mail
 * @param {string} senderName The name of the sender
 * @param {string} senderEmail The email of the sender. Must first be registered on Mailjet.
 * @param {{}[]} recipients The details of recipients
 * @param {string} htmlContent The html content of the mail
 * @param {string} textContent The text content of the mail
 * @param {object} sqs
 * @param {string} queue
 * throw Error
 * @return {object}
 */

const send = async ({subject, senderName, senderEmail, recipients, htmlContent, textContent}, {sqs, queue}) => {
    if ( !val.isLength(subject, {min: 2, max: 100}) ) {
        throw new Error('sender must be a string of not more than 11 characters');
    }

    if (senderName && !val.isEmail(senderEmail)) {
        throw new Error('senderEmail must be a valid email')
    }

    if (senderName && !val.isLength(senderName, {min: 2, max: 100})) {
        throw new Error('Invalid senderName');
    }

    if (!Array.isArray(recipients) || recipients.length < 1) {
        throw new Error('recipients must be an array of recipients')
    }

    if ( !recipients.every(i => isRecipient(i) === true) ) {
        throw new Error('Invalid recipients detected');
    }

    if (htmlContent && typeof htmlContent !== 'string') {
        throw new Error('Invalid html content');
    }

    if (textContent && typeof textContent !== 'string') {
        throw new Error('Invalid text content');
    }

    const body = JSON.stringify({
        subject,
        senderName,
        senderEmail,
        recipients,
        htmlContent,
        textContent
    });

    const params = {
        MessageBody: body,
        QueueUrl: queue,
        MessageAttributes: {
            messageType: {
                DataType: 'String',
                StringValue: 'email'
            }
        }
    };

    let result;

    try {
        result = await sendToQueue(params, sqs);
    } catch (err) {
        throw err;
        // throw new Error('Unable to send mail to sqs');
    }

    return result;
};

/**
 * @desc validate if an object is a valid recipient
 * @param {string} email
 * @param {string} name
 * @param {object} vars
 */
const isRecipient = ({email, name, vars}) => {
    if ( !(val.isEmail(email) && val.isLength(name, {min: 2, max: 100})) ) {
        return false;
    }

    return !(vars && typeof vars !== 'object');
};

const sendToQueue = (params, sqs) => {
    return new Promise((resolve, reject) => {
        sqs.sendMessage(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};

module.exports =  send;
