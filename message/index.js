'use strict';

const sms = require('./sms');
const email = require('./email');
const pushNotification = require('./push-notifications');

class Message {
    constructor({sqs, queueUrl, googleApplicationCredentials, deviceTokenUrl, basicAuth}) {
        this.sqs = sqs;
        this.queueUrl = queueUrl;
        this.deviceTokenUrl = deviceTokenUrl;
        this.pushNotification = pushNotification(googleApplicationCredentials);
        this.basicAuth = basicAuth;
    }

    async sendSMS({senderName, sender, message, recipients}) {
        return sms({
            senderName,
            sender,
            message,
            recipients
        }, {
            sqs: this.sqs,
            queue: this.queueUrl,
        });
    }

    async sendMail({subject, senderName, senderEmail, recipients, htmlContent, textContent}) {
        return email({
            subject,
            senderName,
            senderEmail,
            recipients,
            htmlContent,
            textContent
        },{
            sqs: this.sqs,
            queue: this.queueUrl,
        });
    }

    sendPushNotification({ eventType, message, meta = {} }) {
        if (!this.pushNotification) throw Error('push notifications service not initialized, provide valid "googleApplicationCredentials"');

        return this.pushNotification({
            eventType,
            message,
            meta,
            deviceTokenUrl: this.deviceTokenUrl,
            basicAuth: this.basicAuth,
        });
    }
}

module.exports = Message;
