'use strict';

const Readable = require('stream').Readable;

const limit = 200;

const transform = (instance) => {
    const transformed = {};

    for (let prop in instance) {
        const key = prop.split('.');
        transformed[key[key.length - 1]] = instance[prop];
    }

    return transformed;
};

const pushStream = (stream, iArr) => {
    let data;
    let notPaused;

    while ((data = iArr.next().value)) {
        if (!stream.push(transform(data))) {
            stream.pause();

            notPaused = false;
        } else {
            notPaused = true;
        }
    }

    return notPaused;
};

function _streamNextChunk(params, modelOptions) {
    console.log('params =====', params);
    let iArr;
    let records;
    let model = this;
    let queryOptions = {
        ...params,
        raw: true,
        limit: params && params.chunkLimit || limit,
    };

    return async function next(stream, readOnly = false) {
        if (stream.page === stream.maxPage) {
            return stream.destroy();
        }

        stream.page = stream.page + 1;
        queryOptions.offset = (stream.page - 1) * queryOptions.limit;

        records = await model.findAll(queryOptions, modelOptions)
            .catch(console.log);

        iArr = records[Symbol.iterator]();

        // Push new records to stream
        pushStream(stream, iArr)
    };
}

const modelStream = function (params, modelOptions) {
    let streamNextChunk = _streamNextChunk.bind(this)(
        params,
        modelOptions,
    );

    let stream = new Readable({
        objectMode: true,
        read(size) {
            streamNextChunk(this);
        },
    });

    let countQueryOpts = { ...params, attributes: [], offset: undefined };
    const chunkLimit = params && params.chunkLimit || limit;

    return this.count(countQueryOpts, modelOptions)
        .then((totalCount) => {
            stream.maxPage = Math.ceil(totalCount / chunkLimit);
            stream.page = 0;
        })
        .then(() => stream);
};

module.exports = modelStream;
