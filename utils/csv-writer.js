'use strict';

const csvWriter = require('csv-write-stream');


module.exports = class Writer {
    constructor() {
        this.writer = csvWriter();
    }

    pipe() {
        return this.writer.pipe.apply(this.writer, arguments);
    }

    on() {
        return this.writer.on.apply(this.writer, arguments);
    }

    once() {
        return this.writer.once.apply(this.writer, arguments);
    }

    emit() {
        return this.writer.emit.apply(this.writer, arguments);
    }

    removeListener() {
        return this.writer.removeListener.apply(this.writer, arguments);
    }

    write(obj) {
        // if .write returns false we have to wait until `drain` is emitted
        if (!this.writer.write(obj)) {
            new Promise((resolve) => this.writer.once('drain', resolve));

            return false;
        }

        return true;
    }

    end() {
        // Wrap it in a promise if you wish to wait for the callback.
        this.writer.end();
    }
};
