const aws = require('./aws');
const Message =  require('./message');
const CsvWriterUtil = require('./utils/csv-writer');
const modelStream = require('./utils/model-stream');

class RibyUtil {
    constructor({
        awsAccessKeyId,
        awsSecretAccessKey,
        messageQueueUrl,
        googleApplicationCredentials,
        deviceTokenUrl,
        basicAuth,
    }) {
        this.awsRegion = 'eu-west-2';

        const { sqs } = aws({
            accessKeyId: awsAccessKeyId,
            secretAccessKey: awsSecretAccessKey,
            region: this.awsRegion
        });

        this.message = new Message({
            sqs,
            queueUrl: messageQueueUrl,
            googleApplicationCredentials,
            deviceTokenUrl,
            basicAuth
        });
    }

    static get csvWriter () {
        return new CsvWriterUtil();
    }

    static get modelStream () {
        return modelStream;
    }
}

module.exports = RibyUtil;
