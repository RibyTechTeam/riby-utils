const AWS = require('aws-sdk');

module.exports = ({accessKeyId, secretAccessKey, region}) => {
    AWS.config.credentials = {
        accessKeyId: accessKeyId,
        secretAccessKey: secretAccessKey,
        region: region
    };

    const sqs = new AWS.SQS({
        apiVersion: '2012-11-05',
        region
    });

    return {
        AWS,
        sqs,
    };
};
