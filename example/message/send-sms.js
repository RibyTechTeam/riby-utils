const RibyUtil = require('../../index');

const ribyUtil = new RibyUtil({
    awsAccessKeyId: '',
    awsSecretAccessKey: '',
    messageQueueUrl: '',
    googleApplicationCredentials: '',
    deviceTokenUrl: '',
    basicAuth: '',
});

(async () => {
    let result;

    try {
        result = await ribyUtil.message.sendSMS({
            message: 'Hello! Testing',
            recipients: ['08162362450']
        });
    } catch (error) {
        return console.error('Failed to send sms', error);
    }

    console.info('SMS sent successfully', result);
})();
