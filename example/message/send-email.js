const fs = require('fs');
//add new file extension html
require.extensions['.html'] = function (module, filename) {
    module.exports = fs.readFileSync(filename, 'utf8');
};

const RibyUtil = require('../../index');

const ribyUtil = new RibyUtil({
    awsAccessKeyId: '',
    awsSecretAccessKey: '',
    messageQueueUrl: '',
    googleApplicationCredentials: '',
    deviceTokenUrl: '',
    basicAuth: '',
});

const Template = require('./email-tmpl.html');

(async () => {
    let result;

    try {
        result = await ribyUtil.message.sendMail({
            subject: 'New Account',
            // senderEmail: 'no-reply@riby.me',
            htmlContent: Template,
            textContent: `Welcome to Riby`,
            recipients: [{
                name: 'Benjamin Eloke',
                email: 'benjamineloke@gmail.com',
                vars: {
                    name: 'Benjamin Eloke',
                    email: 'benjaminelokegmail.com',
                    password: 'Jagabgan',
                    domain: 'https://riby.me'
                }
            }]
        });
    } catch (error) {
        return console.error('Failed to send mail', error);
    }

    console.info('Mail sent successfully', result);
})();
