const RibyUtil = require('../../index');

const ribyUtil = new RibyUtil({
    awsAccessKeyId: '',
    awsSecretAccessKey: '',
    messageQueueUrl: '',
    googleApplicationCredentials: '',
    deviceTokenUrl: '',
    basicAuth: '',
});

// targeted notifications
ribyUtil.message.sendPushNotification({
    eventType: 'RCB.CM.ADMIN.NEW_CON_TYPE',
    message: {
        title: 'New Notification',
        message: 'Hey there, welcome to Riby CoBanking',
    },
}).send(['07001234567']);

// braodcase notifications
ribyUtil.message.sendPushNotification({
    eventType: 'RCB.CM.ADMIN.NEW_CON_TYPE',
    message: {
        title: 'New Notification',
        message: 'Hey there, welcome to Riby CoBanking',
    },
}).broadcast();
